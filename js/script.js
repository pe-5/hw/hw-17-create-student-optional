const student = {
    name: '',
    lastName: '',
    tabel: {},
}

let studentFullName = prompt("Enter student's full name:", "Yevhenii Tarasevych")

student.name = studentFullName.split(" ")[0]
student.lastName = studentFullName.split(" ")[1]

let count = 0
while(true){
    count++
    let subject = prompt(`Enter subject ${count}:`, `subject${count}`)
    if(!subject)
        break 

    let score = +prompt(`Enter rating for ${subject}:`, 0)
    if(!score)
        break

    student.tabel[subject] = score
}
            
console.log(student)

let countLow = 0
let ratingSum = 0
count = 0

for (let key in student.tabel){
    count++
    ratingSum += student.tabel[key]
    if(student.tabel[key] < 4)
        countLow++    
}


if(countLow < 1)
console.log('The student is transferred to the next course')

let ratingAvg = ratingSum/count

if(ratingAvg > 7 && countLow < 1)
    console.log('The student is assigned a scholarship')

